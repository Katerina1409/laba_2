package com.company.clients;

public class Visitor extends BaseVisitor {

    public Visitor(String name){
        super(name);
    }

    public void buy(){}

    public void returnGoods(){}
}

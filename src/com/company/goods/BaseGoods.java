package com.company.goods;

import com.company.departments.BaseDepartment;

public class BaseGoods {
    private String name;
    private boolean hasGuarantee;
    private BaseDepartment department;
    private String price;
    private String company;

    public BaseGoods(String name, boolean hasGuarantee,BaseDepartment department,String price,String company){
        this.name=name;
        this.hasGuarantee=hasGuarantee;
        this.department=department;
        this.price=price;
        this.company=company;
    }

    public boolean isHasGuarantee(){
        return hasGuarantee;
    }

}

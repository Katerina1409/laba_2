package com.company.goods;

import com.company.departments.BaseDepartment;

public class Televisor extends ElectronicDevice{
    private boolean hasGuarantee;
    private String model;

    public Televisor(String name, boolean hasGuarantee, String company, BaseDepartment department, String price, String model){
        super(name, hasGuarantee, department, price, company);
        this.model=model;
    }

    public void on(){}

    public void off(){}

    public void selectChanel(){}

    public boolean isHasGuarantee(){
        return hasGuarantee;
    }
}

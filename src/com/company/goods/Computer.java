package com.company.goods;

import com.company.departments.BaseDepartment;

public class Computer extends ElectronicDevice{
    private boolean hasGuarantee;
    private String ram;

    public Computer(String name, boolean hasGuarantee, String ram, BaseDepartment department, String price, String company){
        super(name, hasGuarantee, department, price, company);
        this.ram=ram;
    }

    public void on(){}

    public void off(){}

    public void loadOS(){}

    public boolean isHasGuarantee(){
        return hasGuarantee;
    }
}

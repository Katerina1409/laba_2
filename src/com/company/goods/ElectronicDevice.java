package com.company.goods;

import com.company.departments.BaseDepartment;

public class ElectronicDevice extends BaseGoods{
    private String name;
    private boolean hasGuarantee;
    private BaseDepartment department;
    private String price;
    private String company;

    public ElectronicDevice(String name, boolean hasGuarantee,BaseDepartment department,String price,String company){
        super(name, hasGuarantee, department, price, company);
    }

    public boolean isHasGuarantee(){
        return hasGuarantee;
    }

}

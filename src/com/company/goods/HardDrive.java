package com.company.goods;

import com.company.departments.BaseDepartment;

public class HardDrive extends ElectronicDevice{
    private boolean hasGuarantee;
    private String volume;

    public HardDrive(String name, boolean hasGuarantee, String volume, BaseDepartment department, String price, String company){
        super(name, hasGuarantee, department, price, company);
        this.volume=volume;
    }

    public void format(){}

    public void copy(){}

    public void delete(){}

    public boolean isHasGuarantee(){
        return hasGuarantee;
    }
}
